ActivityPub example server
==========================


Getting started
---------------

Install requirements (probably in a virtualenv):

    $ pip install django requests

Clone this repository:

    $ git clone https://salsa.debian.org/javad/activitypub-example.git

Run the migrations

    $ cd activitypub
    $ ./manage.py migrate

Run the server

    $ ./manage.py runserver

Testing the federation
----------------------

Testing the federation is a tat trickier.
You can use a reverse proxy to simulate remote servers.

First add two new hosts in your hosts file:

    $ cat /etc/hosts
    ...
    127.0.1.1	danial.local javad.local
    ...

Then add two new virtual hosts for danial.local and javad.local.
Here is an example nginx configuration file to achieve that:

    server {
        listen 80;
        index index.html index.htm;

        server_name danial.local;
        server_name_in_redirect off;

        location / {
            proxy_pass http://127.0.0.1:8000;
            proxy_set_header X-Forwarded-Host $host;
        }
    }

Copy the file and change `danial.local` to `javad.local` and the port to `8001`.
You'll also need two different Django configuration files. So copy `activitypub/settings.py` to `activitypub/settings-javad.py` and change the following values:

    $ cat activitypub/settings-javad.py
    ...
    ACTIVITYPUB_DOMAIN = "danial.local" # or javad.local
    ...
    DATABASES = {
        'default': {
            'ENGINE': 'django.db.backends.sqlite3',
            'NAME': os.path.join(BASE_DIR, 'danial.sqlite3'), # or javad.sqlite3
        }
    }


Now you can run the migrations for danial (default) and javad:

    $ ./manage.py migrate --settings=activitypub.settings
    $ ./manage.py migrate --settings=activitypub.settings-javad

Finally run the two servers in different terminals:

    $ ./manage.py runserver # danial
    $ ./manage.py runserver 8001 --setttings=activitypub.settings-javad

Check that the servers are reachable via the correct hosts:

 - [http://danial.local](http://danial.local)
 - [http://javad.local](http://javad.local)

If all works correctly they should be able to reach each others. To verify that, we need to first create users. The simplest way is to do it via the python shell:

    $ ./manage.py shell
    ...
    >>> from activitypub.models import Person
    >>> danial = Person(username="danial", name="Danial Behzadi")
    >>> danial.save()

danial's actor representation should be available at http://danial.local/@danial.
Now do that same for javad:

    $ ./manage.py shell --settings=activitypub.settings-javad
    ...
    >>> from activitypub.models import Person
    >>> javad = Person(username="javad", name="Javad Nikbakht")
    >>> javad.save()

Again, javad's representation should be available at http://javad.local/@javad.
Let's make danial follow javad:

    $ curl -X POST 'http://danial.local/@danial/outbox' -H "Content-Type: application/activity+json" -d '{"type": "Follow", "object": "http://javad.local/@javad"}'

If everything went fine, we should be able to find javad in danial's following collection and danial in javad's followers collection:

- [http://danial.local/@danial/following](http://danial.local/@danial/following)
- [http://javad.local/@javad/followers](http://javad.local/@javad/followers)

If javad publishes a note as follow:

    $ curl -X POST 'http://javad.local/@javad/outbox' -H "Content-Type: application/activity+json" -d '{"type": "Note", "content": "Good morning!"}'

We should be able to find the new note on danial's instance: http://danial.local/@javad@javad.local/notes

API
---

Create a new note:

    POST /@danial/outbox HTTP/1.1
    Host: social.example.com
    Content-Type: application/activity+json

    {
      "type": "Create",
      "to": "https://social.example.com/@danial/followers",
      "object": {
        "type": "Note",
        "content": "Hello world!"
      }
    }

Create a new note without an activity:

    POST /@danial/outbox HTTP/1.1
    Host: social.example.com
    Content-Type: application/activity+json

    {
      "type": "Note",
      "content": "Hello world!"
    }

Follow someone:

    POST /@danial/outbox HTTP/1.1
    Host: social.example.com
    Content-Type: application/activity+json

    {
      "type": "Follow",
      "object": "https://social.example.com/@javad"
    }
